#!/bin/bash

# Install Plugins and set theme.
build_tools=("naco-siren.gradle-language" "richardwillis.vscode-gradle" "vscjava.vscode-maven")
java=("redhat.java" "vscjava.vscode-java-debug" "vscjava.vscode-java-dependency" "vscjava.vscode-java-pack" "vscjava.vscode-java-test" "GabrielBB.vscode-lombok")
misc=("TobiasTao.vscode-md" "VisualStudioExptTeam.vscodeintellicode" "SonarSource.sonarlint-vscode")
platform=("ms-azuretools.vscode-docker" "4ops.terraform" "secanis.jenkinsfile-support")
springboot=("Pivotal.vscode-spring-boot" "vscjava.vscode-spring-boot-dashboard" "vscjava.vscode-spring-initializr")
themes=("gerane.Theme-Monokai-Soft-MD" "sdras.night-owl")

function install_plugins() {
	plugins=("$@")
	for i in "${plugins[@]}"; 
	do
		 code --install-extension "$i"
	done
}

install_plugins "${build_tools[@]}"
install_plugins "${java[@]}"
install_plugins "${misc[@]}"
install_plugins "${platform[@]}"
install_plugins "${springboot[@]}"
install_plugins "${themes[@]}"










